#ifndef SHARED_RESOURCES_H_INCLUDED
#define SHARED_RESOURCES_H_INCLUDED

/**
  ******************************************************************************
  * File Name          : app.c
  * Description        : Contains shared variables for use in multiple files
  ******************************************************************************
  */

#include "stm32f1xx_hal.h"

extern TIM_HandleTypeDef htim2;



#endif // SHARED_RESOURCES_H_INCLUDED

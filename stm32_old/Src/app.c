/**
  ******************************************************************************
  * File Name          : app.c
  * Description        : Holds the logic of the developed Application
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"


void app(void);
void rectangleWave(uint16_t freqeuncy);
void sineWave(uint16_t frequency);


void app(void){
  uint16_t frequency = 15000;
  //sineWave(frequency);
  for(frequency = 100; frequency<20000; frequency += 100){
    rectangleWave(frequency);
    HAL_Delay(500);
  }
  while(1);
} 


/**
  * @brief  This function produces a rectangular signal via TIM1 at PA0 if configured
  * @param  frequency: frequency of the produced signal in Hz, should be between 1 and (2^30)-1
  * @retval None
  */
void rectangleWave(uint16_t frequency){
  // set Frequency of the timer reset to 1Hz at maximum period
  uint16_t prescaler = 122;
  // set period to UINT32_T_MAX using ugly hack
  // TODO: replace this with proper constants
  // timer reset should be happening at 1Hz now
  uint16_t period = 0 - 1;
  // set frequency of the timer reset to the given argument
  period /= frequency; 
  // set the duty to half of the period
  uint16_t dutyCycle = period / 2;
  // write the values to the data structure and hardware
  htim2.Init.Prescaler = prescaler;
  htim2.Init.Period = period;
  HAL_TIM_Base_Init(&htim2);
  __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, dutyCycle);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
}


/**
 * @brief This function produces a rectangular signal via TIM1 at PA0 if configured
 * @param frequency: frequency of the produced signal in Hz, should be between 1 and 15000
 * @retval None
 */
void sineWave(uint16_t frequency){
  //enable cycle dutyCycle
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
  DWT->CYCCNT = 0;
  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  htim2.Init.Prescaler = 0;
  htim2.Init.Period = 7;
  HAL_TIM_Base_Init(&htim2);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
  uint32_t interval = 8000000.0 / (frequency * 16);
  uint32_t dutyCycle = 0;
  int8_t countingDirection = 1;

  while(1){
    while(DWT->CYCCNT < interval);
    DWT->CYCCNT = 0;
    dutyCycle += countingDirection;
    if(dutyCycle == 7){
      countingDirection = -1;
    }
    if(dutyCycle == 0){
      countingDirection = 1;
    }
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, dutyCycle);
  }
}

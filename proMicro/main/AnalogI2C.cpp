#include "AnalogI2C.h"
#include "Wire.h"

#define DA_ONlY_MODE 0b01000000

Analog_I2C::Analog_I2C(uint8_t address){
	this->address = address;
	this->currentVoltage = 0;
	Wire.begin(this->address);
	this->setCurrent(this->currentVoltage);
}	


void Analog_I2C::setCurrent(uint8_t current){
	uint8_t data[2];
	data[0] = DA_ONlY_MODE;
	data[1] = current;	
	int length = 2;
	
	Wire.beginTransmission(this->address);
	Wire.write(data, length);
	Wire.endTransmission();

}


uint8_t Analog_I2C::getCurrent(void){
	return this->currentVoltage;
}



/*
 * Library for PCF8591 Module for AD/DA
 * 
 */
#ifndef  ANALOGI2C_H


#define ANALOG_I2C_ADDRESS 0x48

#include <stdint.h>

class Analog_I2C {
	public:
		Analog_I2C(uint8_t address = ANALOG_I2C_ADDRESS);
		void setCurrent(uint8_t current);
		uint8_t getCurrent(void);
	private:
		uint8_t address;
		uint8_t currentVoltage;
};
#endif	

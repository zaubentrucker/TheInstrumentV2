#include <Arduino.h>
#include "avdweb_SAMDtimer.h"
#include "sound.h"


const float volume_factor = 0.5;

const uint8_t button0_pin = 9;
const uint8_t button1_pin = 0;
const uint8_t button2_pin = 3;
const uint8_t button3_pin = 10;
const uint8_t button4_pin = 7;


uint8_t getCurrentTone(void){
  uint8_t res = 0;
  res |= (!digitalRead(button0_pin)) << 0;
  res |= (!digitalRead(button1_pin)) << 1;
  res |= (!digitalRead(button2_pin)) << 2;
  res |= (!digitalRead(button3_pin)) << 3;
  res |= (!digitalRead(button4_pin)) << 4;
  
  res -= 2; //no button press is no tone
  if(res >= 14){
    res -= 2; //button mapping above c2 must be shifted down for consistency
  }
  return res;
}


void setup() {
  pinMode(button0_pin, INPUT_PULLUP);
  pinMode(button1_pin, INPUT_PULLUP);
  pinMode(button2_pin, INPUT_PULLUP);
  pinMode(button3_pin, INPUT_PULLUP);
  pinMode(button4_pin, INPUT_PULLUP);
  
  sound_setUp();
  sound_unmute();
}

void loop() {
  uint8_t current_tone = getCurrentTone();
  if(current_tone > 27){
    sound_mute();
  }
  else{
    sound_unmute();
    sound_setTone(current_tone);
  }
}

import tkinter


root = tkinter.Tk()

delay_var = tkinter.StringVar()
delay_display = tkinter.Label(root, textvariable=delay_var)

buffsize_var = tkinter.StringVar()
buffsize_display = tkinter.Label(root, textvariable=buffsize_var)


frequency_var = tkinter.StringVar()
frequency_setter = tkinter.Entry(root, textvariable=frequency_var)


def setValues(event):
    delay = float(delay_var.get()) / 1000000
    frequency = float(frequency_var.get())
    buffsize = 1 / (delay * frequency)

    buffsize_var.set(buffsize)


slider = tkinter.Scale(root, from_=40, to=100, variable=delay_var, command=setValues)


delay_display.pack()
buffsize_display.pack()
frequency_setter.pack()
slider.pack()

root.mainloop()
